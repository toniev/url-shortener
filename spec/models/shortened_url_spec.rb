require './models/shortened_url'

describe ShortenedUrl do
  after :each do
    ShortenedUrl.destroy_all
  end

  let(:url) { 'https://www.website.com' }

  it 'can shorten urls' do
    expect(ShortenedUrl.shorten_url(url)).not_to eq('https://www.website.com')
    expect(ShortenedUrl.shorten_url(url).length).to be < 'https://www.website.com'.length
  end

  it 'does not duplicate already shortened urls' do
    expect(ShortenedUrl.shorten_url(url)).to eq(ShortenedUrl.shorten_url(url))
  end

  it 'validates if string is url' do
    expect(ShortenedUrl.url_is_valid?(url)).to eq true
    expect(ShortenedUrl.url_is_valid?(url)).to eq true
  end

  it 'validates if string is url' do
    expect(ShortenedUrl.url_is_valid?('www.google.com')).to eq false
  end
end