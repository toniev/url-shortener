# Url Shortener Code Test

Hello, this is my submission for the url-shortener.

It is deployed on Heroku at: https://shorten-url-service.herokuapp.com/.

To shorten a url you can do:

  `curl https://shorten-url-service.herokuapp.com/ -XPOST -d '{ "url": "https://www.farmdrop.com" }'`
  or go directly to the app's page and do it from the interface.

If you want to run it locally do:

Make sure you have `ruby 2.3.3` and PostgreSQL.

Do a `bundle install`

Do a `rake db:setup`

Run the server with `shotgun`

You can now query the app at localhost:9393.

I didn't get into making a complex shortening algorithm because I don't think this is the point of the task.
I am aware that some more validations when creating the urls can be made just didn't think that it was necessary
for this occasion.
Also I didn't add any tests to test the endpoints, just for the model. If you want I can add some tests for the
endpoints, too.
