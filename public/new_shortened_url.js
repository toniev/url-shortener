$('#submit').click(function(event){
  event.preventDefault();
  var longUrl = $('#long-url').val();

  $.ajax({
    url: '/',
    method: 'POST',
    data: JSON.stringify({url: longUrl}),
    dataType: 'json'
  }).done(function(data){
    short_link = $('<a>').attr('href', '/' + data['short_url']).text(window.location.href + data['short_url']);
    $('.result').html(short_link);
  });
});

$('#long-url').focusout(function(ev){
  url = ev.target.value
  if (url && !/^(f|ht)tps?:\/\//i.test(url)) {
    ev.target.value = 'http://' + url;
  }
});