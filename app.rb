require 'rubygems'
require 'sinatra'
require 'sinatra/activerecord'
require 'slim'

require './models/shortened_url'

set :database_file, 'config/database.yml'

get '/' do
  slim :new_shortened_url
end

post '/' do
  content_type :json
  long_url = JSON.parse(request.body.read)['url']
  short_url = ShortenedUrl.shorten_url(long_url)

  if short_url.nil?
    {status: 422, message: 'Enter valid url'}.to_json
  else
    {short_url: short_url, url: long_url}.to_json
  end
end

get '/:url' do
  shortened = ShortenedUrl.where(short_url: params[:url]).take

  if shortened.nil?
    redirect '/'
  else
    redirect shortened.long_url
  end
end