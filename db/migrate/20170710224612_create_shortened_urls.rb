class CreateShortenedUrls < ActiveRecord::Migration[5.1]
  def change
    create_table :shortened_urls do |t|
      t.string :long_url, null: false, default: ''
      t.string :short_url, null: false, default: ''

      t.timestamps null: false
    end
  end
end
