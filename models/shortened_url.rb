class ShortenedUrl < ActiveRecord::Base
  validates :short_url, presence: true, uniqueness: true
  validates :long_url, presence: true

  class << self
    def shorten_url url
      return nil unless url_is_valid?(url)

      shortened = ShortenedUrl.where(long_url: url).take

      shortened ||= ShortenedUrl.create(long_url: url, short_url: random_string(url))

      shortened.short_url
    end

    def random_string url
      (rand(100000)+url.length).to_s(36)
    end

    def url_is_valid? url
      result = url =~ /\A#{URI::regexp(['http', 'https'])}\z/

      result.nil? ? false : true
    end
  end
end